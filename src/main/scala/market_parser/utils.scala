package market_parser

import akka.http.scaladsl.model.Uri

/**
  * Created by ibes on 24.03.17.
  */
package object utils {
  implicit class UriUtil(uri: Uri) {
    def host: String = uri.scheme + uri.authority
  }

}
