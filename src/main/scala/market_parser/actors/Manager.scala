package market_parser.actors

import java.net.URL

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.actor.Actor.Receive
import akka.http.scaladsl.model.Uri
import akka.stream.ActorMaterializer
import market_parser._
import market_parser.utils.UriUtil

import scala.concurrent.Future

/**
  * Created by Mikekeke on 22-Mar-17.
  */
class Manager(implicit val system: ActorSystem, implicit val mat: ActorMaterializer)
  extends Actor
  with ActorLogging{

  implicit private val manager = self

  var crawlers = Map.empty[String, ActorRef]

  override def receive: Receive = {
    case ParseMarket =>
      startParsingMarket
      log.debug("Starting community parsing")
    case any => log.warning(s"$any")

  }

  private def startParsingMarket = {
    val crawler = crawlers.getOrElse(CommunityParser.NAME, {
      // !!! context to spawn children of this actor!!!
      // can do then 'context.children', and 'context.watch(child)' to catch 'akka.actor.Terminated(ref)' messages from children when they stops
      val crawlerActor = context.actorOf(CommunityParser.props)
      crawlers += (CommunityParser.NAME -> crawlerActor)
      crawlerActor
      context.watch(crawlerActor)
    })

    crawler ! CommunityParser.START
  }
}

object Manager {
  def props()(implicit system: ActorSystem, mat: ActorMaterializer) = Props(new Manager())
  val NAME = Manager.getClass.getSimpleName
}
