name := "ScalaActiors"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.4.16",
  "com.typesafe.akka" %% "akka-http" % "10.0.0",
  "org.scala-lang.modules" % "scala-xml_2.12" % "1.0.6",
  "org.jsoup" % "jsoup" % "1.9.1",
  "net.sourceforge.htmlcleaner" % "htmlcleaner" % "2.6.1"
)


    