package crawler.spiders

import akka.NotUsed
import akka.actor.{ActorSystem, Cancellable}
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.HostConnectionPool
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source}

import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success, Try}
import scala.concurrent.duration._

object Test extends App{
  implicit val system: ActorSystem = ActorSystem("CrawlerSystem")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

//  testHttp
  testSource


  def testSource = {
    val s1: Source[String, Cancellable] = Source.tick(0 seconds, 2 seconds, "test")
    val s2: Source[Int, NotUsed] = Source(List(1,2,3,4))
    val s3 = s1.zipWith(s2)((_, x) => x)
    s3 runWith Sink.foreach(println)
  }

  def testHttp = {
    val result =
      Http().singleRequest(request = HttpRequest(uri="https://mail.ru"))
        .map(resp => {resp.discardEntityBytes(); resp.status.toString})
    val res = Await.result(result, 10 seconds)
    println(res)
  }

}
