package market_parser

import akka.actor.{ActorSystem, PoisonPill}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import market_parser.actors.Manager

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

/**
  * Created by Mikekeke on 22-Mar-17.
  */
object Parser extends App{
  // config
//  import java.io.File
//  import com.typesafe.config.{ Config, ConfigFactory }
//  val config = ConfigFactory.parseFile(new File("./conf/application.conf"))
//  println(config.getString("user-agent-header"))


  implicit val system: ActorSystem = ActorSystem("CrawlerSystem")
  implicit val materializer = ActorMaterializer()
//  implicit val executionContext = system.dispatcher

  val managerActor = system.actorOf(Manager.props())
  implicit val timeout = Timeout(5 seconds)
  managerActor ! ParseMarket

  val printResult: Any => Unit = {
    case Success(s) => println(s"Top level result: $s")
    case Failure(e) => println(s"Top level result: $e")
  }

  Thread.sleep(1000000)
  managerActor ! PoisonPill
  system.terminate()

}
