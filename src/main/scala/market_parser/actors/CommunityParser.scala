package market_parser.actors

import java.net.URL

import akka.actor.{Actor, ActorLogging, Props}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import market_parser.ParsingError
import org.htmlcleaner.{HtmlCleaner, TagNode}
import org.jsoup.Connection

import scala.concurrent.Future


class CommunityParser(implicit val materializer: ActorMaterializer) extends Actor with ActorLogging {

  import CommunityParser._
  import akka.pattern.pipe
  import context.dispatcher

  val cleaner = new HtmlCleaner


  val http = Http(context.system)

  override def receive: Receive = {
    case `START` =>
      Future(cleaner.clean(new URL(mURL))) recover { case e: Exception => ParsingError(e) } pipeTo self
    // TODO: figure out settings
    case rootNode: TagNode =>
      val links = rootNode.getElementListByAttValue("class", "market_listing_row_link", true, true)

      val p = links.toArray(Array.empty[TagNode])
      p map (node => node.getAttributeByName("href")) foreach println

    case err: ParsingError =>
      println(err)

    //      val request = HttpRequest(uri=URL)
    //      http.singleRequest(request).map((request.uri, _)).pipeTo(self)
    //    case (uri, HttpResponse(StatusCodes.OK, headers, entity, _)) =>
    //      entity.dataBytes.runFold(ByteString(""))(_ ++ _).foreach { body =>
    //        val bodyString = body.utf8String
    //      }
    //    case (uri, resp @ HttpResponse(code, _, _, _)) =>
    //      log.info(s"Request failed to $uri, response code: $code")
    //      resp.discardEntityBytes()

    case any =>
      println(s"Uncat $any")
      println(s"Uncat ${any.getClass}")
  }
}

object CommunityParser {
  def props(implicit mat: ActorMaterializer) = Props(new CommunityParser())

  val NAME = CommunityParser.getClass.getSimpleName
  val START = NAME + " start!"
  val mURL = "https://steamcommunity.com/market/search?appid=570"

  case class MarketResponse(response: Connection.Response)

}
