package market_parser

import java.net.URL

import akka.http.scaladsl.model.Uri

import scala.util.Try
import scala.xml.Elem

/**
  * Created by Mikekeke on 23-Mar-17.
  */
sealed trait Message
case object ParseMarket extends Message
case class ParsingError(e: Throwable) extends Message

