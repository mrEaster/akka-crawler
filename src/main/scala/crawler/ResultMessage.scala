package crawler

import akka.http.scaladsl.model.HttpResponse

/**
  * Created by ibes on 15.03.17.
  */
sealed trait ResultMessage
case class Ok(status: String, info: String) extends ResultMessage
case class Bad(status: String, info: String) extends ResultMessage
case class Error(e: Throwable) extends ResultMessage

object ResultMessage {
  val OK = HttpResponse(200).status.toString

  def apply(status: String, info: String): ResultMessage =
    if (OK == status) Ok(status, info) else Bad(status,info)

  def apply(e: Throwable): ResultMessage = Error(e)
}