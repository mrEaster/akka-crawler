package crawler.spiders

import akka.NotUsed
import akka.actor.{ActorRef, ActorSystem, Cancellable}
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.HostConnectionPool
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source}
import crawler.ResultMessage

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success, Try}
import scala.concurrent.duration._

/**
  * Created by Mikekeke on 26-Feb-17.
  */
case class Spider(id: Int, url: String, paths:Seq[String])
                 (implicit val system: ActorSystem,
                  implicit val materializer: ActorMaterializer,
                  implicit val executionContext: ExecutionContextExecutor,
                  implicit val respCollector: ActorRef) {
  println(s"Spider #$id created")

  val source: Source[String, NotUsed] =
    if (paths.nonEmpty) Source(List(paths).flatten) else Source.single("/")
  val tickSource =
    Source.tick(0 seconds, 5 seconds, "").zipWith(source)((_, path) => path)

  val poolClientFlow = Http().cachedHostConnectionPool[String](url)
  val toRequestFlow = Flow[String].map[(HttpRequest, String)](s => HttpRequest(uri = s) -> s)
  val requestFlow = toRequestFlow via  poolClientFlow


  val sink = Sink.foreach[(Try[HttpResponse], String)](el => {
    el._1 match {
      case Success(resp) =>
        respCollector ! ResultMessage(resp._1.toString(), s"Spider #$id: Request to host $url to ${el._2}:")
      case Failure(e) =>
        respCollector ! ResultMessage(e)
    }
  })

//  source via httpFlow via poolClientFlow runWith sink
  def run = tickSource via requestFlow runWith sink

}


