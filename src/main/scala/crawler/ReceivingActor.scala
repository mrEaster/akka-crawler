package crawler

import akka.actor.Actor
import crawler.Ok

import scala.collection.mutable.ListBuffer

/**
  * Created by ibes on 15.03.17.
  */
class ReceivingActor extends Actor{
  private var responces = Map.empty[String, String]
  private var errors = Map.empty[String, String]
  private var fails = new ListBuffer[Throwable]()

  override def receive: Receive = {

    case Ok(s, r) =>
      println("Receiving")
      responces += (s -> r)
    case Bad(s, r) =>
      println("Receiving")
      errors += (s -> r)
    case Error(e) =>
      println("Receiving")
      fails += e
  }

  @scala.throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    super.postStop()
    println("-Stats-")
    println("Succeeded:")
    responces.foreach(println)
    println("Bad:")
    errors.foreach(println)
    println("Failed with error:")
    fails.foreach(_.printStackTrace)
  }
}
