package market_parser

import java.net.URL

import akka.http.javadsl.model.headers.UserAgent
import akka.http.scaladsl.model.HttpHeader.ParsingResult
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{HttpHeader, Uri}

import scala.util.matching.Regex

/**
  * Created by Mikekeke on 22-Mar-17.
  */

import akka.http.scaladsl.model.headers._

package object storage {
//    val marketUrl: Uri = Uri("https://market.dota2.net/?s=price&rs=0;100000")
  val marketUrl: Uri = Uri("http://quotes.toscrape.com/page/1/")

  implicit val head: Regex = """(.+):(.*)""".r
  implicit val userAent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 OPR/43.0.2442.1144"
  implicit def string2headers(s:String)(implicit r: Regex, agent: String): List[HttpHeader] =
    s.split("\n").map {s =>
      val r(k, v) = s
      (k, v) match {
        case ("Host", _) => Host("market.dota2.net")
//        case ("User-Agent", _) => createAgent
        case _ => RawHeader(k, v)
      }
    }.toList

  def createAgent = UserAgent.create(
    ProductVersion("Mozilla", "5.0"),
    ProductVersion("Chrome", "56.0.2924.87")
  )

  val headersFromBrowser =
    """Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Encoding:gzip, deflate, sdch, br
Accept-Language:en-US,en;q=0.8
Cache-Control:max-age=0
Connection:keep-alive
Referer:https://market.dota2.net/
Save-Data:on
Upgrade-Insecure-Requests:1"""

//  val sHeaders: List[RawHeader] = headersFromBrowser

}
