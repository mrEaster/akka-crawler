package crawler

import akka.actor.{ActorSystem, PoisonPill, Props}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}

import scala.concurrent._
import akka._
import akka.actor.FSM.Failure
import akka.actor.Status.Success
import akka.actor._
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.HostConnectionPool
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.stream._
import akka.stream.scaladsl._
import akka.util._
import crawler.spiders.Spider

import scala.collection.immutable.Seq
import scala.concurrent.duration._

import scala.util.Try

/**
  * Created by Mikekeke on 22-Feb-17.
  */
object Crawler extends App {
  implicit val system: ActorSystem = ActorSystem("CrawlerSystem")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher
  implicit val collectorActor: ActorRef = system.actorOf(Props[ReceivingActor])

  val urls: Seq[(Request, Int)] = List(
    Request("mail.ru", Seq.empty),
    Request("rambler.ru", Seq.empty),
    Request("akka.io", Seq("/", "/zxc"))
  ) zip (Stream from 1)

  urls.foreach {
    case (request, idx) => Future(Spider(idx, request.url, request.api).run)
  }

  system.scheduler.scheduleOnce(10 seconds) {
    collectorActor ! PoisonPill
    system.terminate()
  }
}

case class Request(url: String, api: Seq[String])
